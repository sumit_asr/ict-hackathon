function injectTheSetScript(profileName) {
 chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
  chrome.tabs.executeScript(tabs[0].id, {
    code: 'var profileName = "' + profileName + '"'
  }, function() {
    chrome.tabs.executeScript(tabs[0].id, {file: "set_data.js"});
  }); 
 });
}

function injectTheLoadScript(profileName) {
 chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
    chrome.tabs.executeScript(tabs[0].id, {
      code: 'var profileName = "' + profileName + '"'
    }, function() {
      chrome.tabs.executeScript(tabs[0].id, {file: "load_data.js"});
    });
 });
}

document.addEventListener('DOMContentLoaded', function() {

 var cookieList = getCookie("ProfileList");
 var profiles = [];
 if(!isEmpty(cookieList)){
   profiles = JSON.parse(getCookie("ProfileList"));
 }
 console.log(profiles);
 var profileselect = document.getElementById("profiles");
 for(var p=0; p<profiles.length; p++){
    var opt = document.createElement('option');
    opt.value = profiles[p];
    opt.innerHTML = profiles[p];
    profileselect.appendChild(opt);
 }
 var link = document.getElementById('clickactivity');
 // onClick's logic below:
 link.addEventListener('click', function() {
  var profileselect = document.getElementById("profiles");
  var profileName = profileselect[profileselect.selectedIndex].value;
  injectTheSetScript(profileName);
 });

 var load = document.getElementById('loadclickactivity');
 load.addEventListener('click', function() {
  var profileselect = document.getElementById("profiles");
  var profileName = profileselect[profileselect.selectedIndex].value;
  injectTheLoadScript(profileName);
 });
 
 var add = document.getElementById("addUser")
 add.addEventListener("click", function(){
    var profileName = document.getElementById("register_user").value;
    profiles.push(profileName); 
    setCookie("ProfileList", JSON.stringify(profiles), 30);
    console.log(profileName);
    var opt = document.createElement('option');
    opt.value = profileName;
    opt.innerHTML = profileName;
    profileselect.appendChild(opt);
 });
});

function setCookie(cname,cvalue,exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires=" + d.toGMTString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}
 
function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function isEmpty(arr) {
    return (!arr || 0 === arr.length);
}

