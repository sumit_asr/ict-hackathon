console.log("profileName: " + profileName);
var filters = [];
var selectedBrands = getSelectedBrands();
filters[0] = selectedBrands;
filters[1] = selectedShoeSize();
filters[2] = shoeColorSave();
console.log("colors saved "+filters[2]);
if(ifTopBrands()){
    filters.push('TopBrands');
}
if(ifPrime()){
    filters.push('Prime');
}
if(ifFulfilled()){
    filters.push('Fulfilled');
}
if(isOutOfStock()){
  filters.push('oos');
}

if(document.getElementById("low-price")) {
  var low_price = document.getElementById("low-price").value;
}
if(document.getElementById("high-price")) {
  var high_price = document.getElementById("high-price").value;
}
if(low_price) {
  var lowp = "low-price*"+low_price;
  filters.push(lowp);
}
if(high_price) {
  var highp = "high-price*"+high_price;
  filters.push(highp);
}

// chrome.storage.sync.set({'A':filters}, function() {
//     // Notify that we saved.
//     console.log('Settings saved');
// });

saveData(profileName);

function saveData(profileName){
	console.log("profileName: " + profileName);
	console.log("filters: " + filters);

	var profileInfo = {[profileName]: filters};
	console.log("profileInfo: " + JSON.stringify(profileInfo));
	localStorage.setItem(profileName, JSON.stringify(profileInfo));
	console.log('Settings saved');
}


function getSelectedBrands() {
var arr=[];
var i=0;
if(document.getElementsByClassName("groupMultiSel")!=undefined && document.getElementsByClassName("groupMultiSel")[0].children[i]!=undefined)
{
	while(i<300 && document.getElementsByClassName("groupMultiSel")[0].children[i]!=undefined && document.getElementsByClassName("groupMultiSel")[0].children[i].getAttribute("class")!=="firstUnselRefVal")
	{
	 arr.push(document.getElementsByClassName("groupMultiSel")[0].children[i].children[0].title);
	 i++;
	}
	if(i>50){ // Hack !!!!! DO NOT CHANGE
	  i=0;
	  arr=[];
	}
	return arr;
	}
}

function isOutOfStock() {
  var oos=false;
  var a = document.getElementById("ref_1318483031").children[0].children[0].children[0].getAttribute("src").indexOf("unselected");
  if(a === -1) {
    oos=true;
  }
  return oos;
}

function makePrime() {
  var next=document.getElementById('ref_10440598031').children[0].getAttribute("class");
	var prime=false;
	var num="";
	if(next==="refinementImage")
	{
		num=document.getElementById('ref_10440598031').children[0].children[0].click();
	}
	else
	{
		num=document.getElementById('ref_10440598031').children[1].children[0].click();
	}
}
function ifPrime(){
  var next=document.getElementById('ref_10440598031').children[0].getAttribute("class");
	var prime=false;
	var num="";
	if(next==="refinementImage")
	{
		num=document.getElementById('ref_10440598031').children[0].children[0].children[0].getAttribute("src").indexOf("unselected");
	}
	else
	{
		num=document.getElementById('ref_10440598031').children[1].children[0].children[0].getAttribute("src").indexOf("unselected");
	}
	if(num=== -1)
	{
		prime=true;
	}
	return prime;
}

function makeFulfilled() {
  var next=document.getElementById('ref_10440596031').children[0].getAttribute("class");
	var prime=false;
	var num="";
	if(next==="refinementImage")
	{
		num=document.getElementById('ref_10440596031').children[0].children[0].click();
	}
	else
	{
		num=document.getElementById('ref_10440596031').children[1].children[0].click();
	}
}
function ifFulfilled() {
  var next2=document.getElementById('ref_10440596031').children[0].getAttribute("class");
	var fulfilled=false;
	if(next2==="refinementImage")
	{
		num=document.getElementById('ref_10440596031').children[0].children[0].children[0].getAttribute("src").indexOf("unselected");
	}
	else
	{
		num=document.getElementById('ref_10440596031').children[1].children[0].children[0].getAttribute("src").indexOf("unselected");
	}
	if(num=== -1)
	{
		fulfilled=true;
	}
	return fulfilled;
}

function makeTopBrands() {
  var next=document.getElementById('ref_11301362031').children[0].getAttribute("class");
	var prime=false;
	var num="";
	if(next==="refinementImage")
	{
		num=document.getElementById('ref_11301362031').children[0].children[0].click();
	}
	else
	{
		num=document.getElementById('ref_11301362031').children[1].children[0].click();
	}
}
function ifTopBrands() {
  var element=document.getElementById('ref_11301362031');
  if(!element){
    return false;
  }
  next3=element.children[0].getAttribute("class");
	var topBrands=false;
	if(next3==="refinementImage")
	{
		num=document.getElementById('ref_11301362031').children[0].children[0].children[0].getAttribute("src").indexOf("unselected");
	}
	else
	{
		num=document.getElementById('ref_11301362031').children[1].children[0].children[0].getAttribute("src").indexOf("unselected");
	}
	if(num=== -1)
	{
		topBrands=true;
	}
	return topBrands;
}

function selectedShoeSize(){
	/**
		This function will return you two arrays:
		1) The first one is boolean array containining true if the particular size is selected and false if not.
		The array size is 23 based on numbers from 3 to 14 with step difference of 0.5.
		2) The second one gives you the button id of each of the size buttons from 3 to 14.
		This is again an array of 23 size and this contains empty array if the button is unclickable.
	**/
	var shoeSizeArray = [];
	var sizeIdArray = [];
	var shoeNumberSize = 24;
	for(var i=0;i<24;i++){
		shoeSizeArray.push(false);
	}

	var arr = document.getElementById('p_n_size_browse-vebin');
	var a1 = arr.getElementsByClassName("refinementLink selected");
	var outerNodes = arr.getElementsByClassName("buttonsprite");
	for(var i=0;i<outerNodes.length;i++){
		var A = !outerNodes[i].children[0].className;
		if(A){
	        var B = outerNodes[i].children[0].children[0].children[0].className === "refinementLink selected";
	        // console.log(outerNodes[i].title);
	        // console.log(i+": "+A+" "+B);
	        if (A && B){
	            // console.log(outerNodes[i].title);
	            shoeSizeArray[i] = true;
	        }
	    }
	    sizeIdArray.push(outerNodes[i].children[0].id);
	}
	return [shoeSizeArray,sizeIdArray];
}

function shoeColorSave(){
	var result=[];
	//when the length is 1
	// if(document.getElementById("ref_2022042031").children.length === 1) {
	// 	// var arr = document.getElementById("ref_2022042031").children[0].children;
	// 	// //0th child of arr is the style class and the last is the clear button. The number of color may vary.
	// 	// for(var i=0;i<arr.length;i++){
	// 	// 	if(arr[i].className === "colorsprite" && arr[i].children[0].children[0].children[0].className==="refinementLink selected"){
	// 	// 		result.push(arr[i].title);
	// 	// 	}
	// 	// }
	// 	return result;
	// } else
	/**
	Result array contains a list of all the colour titles.
	**/
	if(document.getElementById("ref_2022042031").children.length === 2){
		//here 0th child of the parent array is the clear button and the second one consists of all the colours.
		var arr = document.getElementById("ref_2022042031").children[1].children;
		//0th child of arr is the style class and the last is the clear button. The number of color may vary.
		for(var i=0;i<arr.length;i++){
			if(arr[i].className === "colorsprite" && arr[i].children[0].children[0].children[0].className==="refinementLink selected"){
				if(result.indexOf(arr[i].title) === -1){
					result.push(arr[i].title);
				}
			}
		}
		return result;
	}
}
