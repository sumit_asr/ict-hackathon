var profileInfo = loadData(profileName);
console.log("profileInfo: " + JSON.stringify(profileInfo));
populateViews(profileInfo, profileName);

function populateViews(profileInfo, profileName) {
	console.log("Starting to apply");
    console.log(profileInfo[profileName]);

    setSelectedSizes(profileInfo[profileName][1]);
    var colors = profileInfo[profileName][2];
    if(colors) {
  window.setTimeout(function() {
    for(var i=0;i<colors.length;i++){
         shoeColorFetch(colors[i]);   
          }
   }, 40000);
    }


    if(profileInfo[profileName].length > 0) {
  window.setTimeout(function() {
     setSelectedBrands(profileInfo[profileName][0]);
   }, 10000);
    }
    if(profileInfo[profileName].indexOf('Prime') != -1){
      if(!ifPrime()){
  window.setTimeout(function() {
     makePrime();
   }, 15000);

      }
    }


    if(profileInfo[profileName].indexOf('Fulfilled') != -1){
      if(!ifFulfilled()){
  window.setTimeout(function() {
     makeFulfilled();
   }, 20000);
         
      }
    }
    if(profileInfo[profileName].indexOf('TopBrands') != -1){
      if(!ifTopBrands()){
  window.setTimeout(function() {
     makeTopBrands();
   }, 25000);
         
      }
    }

    if(profileInfo[profileName].indexOf('oos') != -1){
      if(!isOutOfStock()){
  window.setTimeout(function() {
     makeOutOfStock();
   }, 30000);   
      }
    }

    var low_price = document.getElementById("low-price").value;
    var high_price = document.getElementById("high-price").value;

    var lowp;
    var highp;
    var arrayLength = profileInfo[profileName].length;
    for (var i = 0; i < arrayLength; i++) {
    var a = profileInfo[profileName][i];
if(a!==null)
{
    if(a.includes("low-price")) {
      var b = a.split('*');
      lowp = b[1];
    }
    if(a.includes("high-price")) {
      var b = a.split('*');
      highp = b[1];
    }

    var clickPriceGo = false;
    if(lowp && !low_price) {
      document.getElementById("low-price").value = lowp;
      clickPriceGo = true;

    }
    if(highp && !high_price) {
      document.getElementById("high-price").value = highp;
      clickPriceGo = true;
    }

    if(clickPriceGo) {
		var len_ul = document.getElementById("ref_4516629031").children.length;
		var obj_li = document.getElementById("ref_4516629031").children[len_ul-1];
		var table_size = obj_li.children[0].children[0].children.length;
		var table_var = obj_li.children[0].children[0].children[table_size-1];
    window.setTimeout(function() {
       table_var.children[0].children[0].children[4].children[0].click();
     }, 35000);   
    }
}
}
}

function loadData(profileName) {
	var profileInfo = localStorage.getItem(profileName);
	console.log("Loaded " + profileInfo);
	return JSON.parse(profileInfo);
}

function setSelectedBrands(arr) {
  var arrayLength = arr.length;
  var i = 0;
  while(i<arrayLength) {
    var j=0;  
    while(j<400 && document.getElementsByClassName("groupMultiSel")[0].children[j].children[0].getAttribute("title").trim().toLowerCase()!==arr[i].trim().toLowerCase())
    {
     j++;
    }
  if(document.getElementsByClassName("groupMultiSel")[0].children[j].children[0].children[0].getAttribute("src").indexOf("unselected") !== -1) {
    document.getElementsByClassName("groupMultiSel")[0].children[j].children[0].click();
  }
  ++i;
  }
}

function makeOutOfStock() {
	document.getElementById('ref_1318483031').children[0].children[0].click();
}

function isOutOfStock() {
  var oos=false;
  var a = document.getElementById("ref_1318483031").children[0].children[0].children[0].getAttribute("src").indexOf("unselected");
  if(a === -1) {
    oos=true;
  }
  return oos;
}

function makePrime() {
  var next=document.getElementById('ref_10440598031').children[0].getAttribute("class");
	var num="";
	if(next==="refinementImage")
	{
		document.getElementById('ref_10440598031').children[0].children[0].click();
	}
	else
	{
		document.getElementById('ref_10440598031').children[1].children[0].click();
	}
}
function unmakePrime() {
  var next=document.getElementById('ref_10440598031').children[0].getAttribute("class");
	var num="";
	if(next==="refinementClear")
	{
		document.getElementById('ref_10440598031').children[0].children[0].click();
	}
}
function ifPrime(){
  var next=document.getElementById('ref_10440598031').children[0].getAttribute("class");
	var prime=false;
	var num="";
	if(next==="refinementImage")
	{
		num=document.getElementById('ref_10440598031').children[0].children[0].children[0].getAttribute("src").indexOf("unselected");
	}
	else
	{
		num=document.getElementById('ref_10440598031').children[1].children[0].children[0].getAttribute("src").indexOf("unselected");
	}
	if(num=== -1)
	{
		prime=true;
	}
	return prime;
}

function makeFulfilled() {
  var next=document.getElementById('ref_10440596031').children[0].getAttribute("class");
	var prime=false;
	var num="";
	if(next==="refinementImage")
	{
		num=document.getElementById('ref_10440596031').children[0].children[0].click();
	}
	else
	{
		num=document.getElementById('ref_10440596031').children[1].children[0].click();
	}
}
function ifFulfilled() {
  var next2=document.getElementById('ref_10440596031').children[0].getAttribute("class");
	var fulfilled=false;
	if(next2==="refinementImage")
	{
		num=document.getElementById('ref_10440596031').children[0].children[0].children[0].getAttribute("src").indexOf("unselected");
	}
	else
	{
		num=document.getElementById('ref_10440596031').children[1].children[0].children[0].getAttribute("src").indexOf("unselected");
	}
	if(num=== -1)
	{
		fulfilled=true;
	}
	return fulfilled;
}

function makeTopBrands() {
  var next=document.getElementById('ref_11301362031').children[0].getAttribute("class");
	var prime=false;
	var num="";
	if(next==="refinementImage")
	{
		num=document.getElementById('ref_11301362031').children[0].children[0].click();
	}
	else
	{
		num=document.getElementById('ref_11301362031').children[1].children[0].click();
	}
}
function ifTopBrands() {
  var next3=document.getElementById('ref_11301362031').children[0].getAttribute("class");
	var topBrands=false;
	if(next3==="refinementImage")
	{
		num=document.getElementById('ref_11301362031').children[0].children[0].children[0].getAttribute("src").indexOf("unselected");
	}
	else
	{
		num=document.getElementById('ref_11301362031').children[1].children[0].children[0].getAttribute("src").indexOf("unselected");
	}
	if(num=== -1)
	{
		topBrands=true;
	}
	return topBrands;
}

function setSelectedSizes(arr) {
  var shoeSizeArray = arr[0];
  var sizeIdArray = arr[1];
  for(var i=0;i<24;i++){
		if(shoeSizeArray[i]) {
      var element = document.getElementById(sizeIdArray[i]);
      if(element && element.children[0].getAttribute("class") !== "selected") {
        element.click();
      }
    }
	}
}

function shoeColorFetch(colorTitle){
	var lengthParentArray = document.getElementById("ref_2022042031").children.length;
	var arr = document.getElementById("ref_2022042031").children[lengthParentArray-1].children;
	for(var i=0;i<arr.length;i++){
 		if(arr[i].className === "colorsprite" && arr[i].title===colorTitle){
      if(arr[i].children[0].children[0].getAttribute("class") !== "selected") {
         arr[i].children[0].click();
      }
		}
	}
}
